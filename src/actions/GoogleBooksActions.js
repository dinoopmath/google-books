 import axios from 'axios';

import {GOOGLE_BOOKS_VOLUMES_URL,GOOGLE_BOOKS_API_KEY} from "../utils/Constants";

export function getGoogleBooksList(searchText) {

    return {
        type: "GET_BOOKS",
        payload: new Promise((resolve, reject) => {
            let url = `${GOOGLE_BOOKS_VOLUMES_URL}q=${searchText}&key=${GOOGLE_BOOKS_API_KEY}`;
            axios.get(url).then(function(response){
                console.log(response.data);
                response.data.searchText = searchText;
                resolve(response.data);
            });
              
        })
    };
}
