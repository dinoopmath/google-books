import React from "react";
import {connect} from "react-redux";

import Header from "../components/Header";
import Footer from "../components/Footer";

import { getGoogleBooksList} from "../actions/GoogleBooksActions";
import { startLoading,endLoading } from "../actions/LoadingActions"; 
import GoogleBooksList from "../containers/GoogleBooksListContainer";

class Home extends React.Component {

    componentDidMount(){

       const { match: { params } } = this.props;

       this.props.init(params.searchtext || '');
    }
    render() {
        return (
            <div>
              <Header />
              <main id="main-content">
                  <GoogleBooksList books={this.props.books} loading={this.props.loading}  /> 
              </main>    
              <Footer />
            </div>  
        );
    }
}

const mapStateToProps = ({books, loading }) => ({
    books,
    loading
});

const mapDispatchToProps = (dispatch) => ({
    init: (searchtext) => {
      
      if(searchtext){
          dispatch(startLoading());
          dispatch(getGoogleBooksList(searchtext)).then(()=>{
              dispatch(endLoading());
          })
      }    
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
