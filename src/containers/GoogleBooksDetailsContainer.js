import React from "react";
import {connect} from "react-redux";

import Header from "../components/Header";
import Footer from "../components/Footer";

import GoogleBooksDetails from "../components/GoogleBooksDetails";



class GoogleBooksDetailsPage extends React.Component {

    componentDidMount(){
    }
    render() {
        return (
            <div>
              <Header />
              <main id="main-content">
                  <GoogleBooksDetails books={this.props.books} bookid={this.props.match.params.bookid} /> 
              </main>    
              <Footer />
                
            </div> 
        );
    }
}


const mapStateToProps = ({books}) => ({
    books
});


const mapDispatchToProps = (dispatch) => {
    return {
        init: (id) => {
        }

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(GoogleBooksDetailsPage);
