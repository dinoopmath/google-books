import {connect} from "react-redux";
import SearchForm from "../components/SearchForm";

import { getGoogleBooksList} from "../actions/GoogleBooksActions";

import { startLoading,endLoading } from "../actions/LoadingActions"; 

import createHistory from "history/createBrowserHistory";
const history = createHistory();


const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    searchHandler:(e,serchText)=>{
       dispatch(startLoading());
       dispatch(getGoogleBooksList(serchText)).then((res)=>{
        debugger;
          history.push(`/list/${serchText}`)
          dispatch(endLoading());
       });
      
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchForm)