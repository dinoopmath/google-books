  const LoadingReducer = (state = {
}, action) => {
    switch (action.type) {
        case "START_LOADING":
             state = {
                ...state,
                isLoading: true
            };
            break;
        case "END_LOADING":
             state = {
                ...state,
                isLoading: false
            };
            break;            
        default:
           break;              
    }
    return state;
};
export default LoadingReducer;