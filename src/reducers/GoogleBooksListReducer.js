const GoogleBooksListReducer = (state = {
}, action) => {
    switch (action.type) {
        case "GET_BOOKS_FULFILLED":
             state = {
                ...state,
                books: action.payload
            };
            break;
        default:
           break;              
    }
    return state;
};
export default GoogleBooksListReducer;