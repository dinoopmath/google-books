import React from 'react';
import ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';


import "./styles/style.scss";
import { Provider } from "react-redux";
import { Route } from "react-router";

import {
  ConnectedRouter
} from "react-router-redux";


import Home from "./containers/Home";
import GoogleBooksDetails from "./containers/GoogleBooksDetailsContainer";

import store from "./store";

import createHistory from "history/createBrowserHistory";
const history = createHistory();



ReactDOM.render(
  <Provider store={store}>
	  <ConnectedRouter history={history}>
		<div>
	        <Route path="/" exact component={Home} />
	        <Route path="/list/:searchtext"  component={Home} />
	     	<Route path="/bookdetails/:bookid" component={GoogleBooksDetails}/>

		</div>
	  </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();