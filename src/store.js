import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";
import books from "./reducers/GoogleBooksListReducer";

import loading from "./reducers/LoadingReducer";

import {routerMiddleware} from "react-router-redux";



import createHistory from "history/createBrowserHistory";

const history = createHistory();

const middleware = routerMiddleware(history);


export default createStore(
  combineReducers({
        books,
        loading
  }),
  applyMiddleware(logger, thunk, promise(),middleware)
);



