import React from 'react'
import Img from 'react-image'
import { Link } from 'react-router-dom'

const GoogleBooksDetails = ({books,bookid}) => {

   let book,imageURL;
   
   if(books.books){

   		book = books.books.items.filter(book=>book.id===bookid);
     
        if(book[0].volumeInfo.imageLinks){
          	  imageURL = book[0].volumeInfo.imageLinks.smallThumbnail;
        }
        else{
          	  imageURL = "http://via.placeholder.com/150x212"
        }

   		return (
				<div className="books-details-wrapper">
					<Link to={`/list/${books.books.searchText}`}>Back</Link>
						<h2>Books Details</h2>
			            <div className="book-tile">
			             	<Img src={imageURL} width="150px" />
			             	<div className="book-info">
			             		<p>
			             			<label>Title:</label><span>{book[0].volumeInfo.title}</span>
			             		</p>

			             		<p>
			             			<label>Author:</label><span>{book[0].volumeInfo.authors ? book[0].volumeInfo.authors.join("") : "N/A" }</span>
			             		</p>

			             		<p>
			             			<label>Publish Date:</label><span>{book[0].volumeInfo.publishedDate}</span>
			             		</p>             		

			             		<p>
			             			<label>Publisher:</label><span>{book[0].volumeInfo.publisher}</span>
			             		</p> 			             		 			             		
			             	</div>
			            </div> 	
		        </div>
		  )
   }
   else{
   		return (<p>No Results</p>)
   }
}


export default GoogleBooksDetails