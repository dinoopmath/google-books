import React from "react";

const Loader = (loading) => {

    let style =  (loading.loader) ? {"display":"block"} : {"display":"none"};

    return (
		<div className="loader" style={style}>
			<div className="loader-spin"></div>
		</div>
    )
  
}  

export default Loader