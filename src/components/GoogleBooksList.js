import React from 'react'
import Img from 'react-image'
import { Link } from 'react-router-dom'

import Loader from "../components/Loader"

const BooksList = ({books,loading}) => {

  let BooksListView;

  if(books.books){

  	    if(!books.books.totalItems){
  	    	return ( <p>No results found</p>)
  	    }	


	   BooksListView = books.books.items.map((book,index)=>{
  
          let imageURL;
          if(book.volumeInfo.imageLinks){
          		imageURL = book.volumeInfo.imageLinks.smallThumbnail;
          }
          else{
          	  imageURL = "http://via.placeholder.com/150x212"
          }
	   
	      return <div key={index} className="book-tile">

	            <Link to={`/bookdetails/${book.id}`}>
	                    
			            
			             	
			                <Img src={imageURL} width="150px" />
			              
			             	<div>
			             		<p>
			             			<label>Title:</label><span>{book.volumeInfo.title}</span>
			             		</p>

			             		<p>
			             			<label>Author:</label><span>{book.volumeInfo.authors ? book.volumeInfo.authors.join("") : "N/A" }</span>authors
			             		</p>

			             		<p>
			             			<label>Publish Date:</label><span>{book.volumeInfo.publishedDate}</span>
			             		</p>             		
			             		
			             	</div>
			        </Link>
			</div>        

	  });


  }

  return (
  		
		<div className="search-results">  
			<Loader loader={loading.isLoading}></Loader>	      
            <div className="book-tile-wrapper">   
	           	{BooksListView}
	   		 </div>    
	   	</div>

  )
}


export default BooksList 

