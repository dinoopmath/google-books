 import React from "react";

const SearchForm = ({searchHandler}) => {

    return (
    	<form className="search-form">
			<input type="type"  className="search-text" placeholder="search books" name="search" />
			<button onClick={(e)=>{
				e.preventDefault();
				let searchText = document.querySelector(".search-text").value; 	
				if(searchText){
					searchHandler(e,searchText);
				}
				
			}}>Search</button>
		</form>
    )
  
}  
     
export default SearchForm    