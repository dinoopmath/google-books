import React from "react";

const Footer = () => {

    return (
		<footer id="site-footer">
			&copy; Google Books Api Demo by Dinoop Mathew
		</footer>
    )
  
}  

export default Footer