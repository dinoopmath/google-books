import React from "react";
import SearchForm from "../containers/SearchFormContainer";

const Header = () => {

    return (
		<header id="site-header">
			<h1>Google Books</h1>
			<SearchForm />
		</header>
    )
  
}  
     
export default Header    